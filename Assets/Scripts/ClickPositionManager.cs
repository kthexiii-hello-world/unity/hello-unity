﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickPositionManager : MonoBehaviour {

    public LayerMask clickMask;

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            Vector3 clickPos = -Vector3.one;

            // METHOD 1: ScreenToWorld
            // clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(0f, 0f, 5f));

            //METHOD 2: Raycast using colliders
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit)) {
                clickPos = hit.point;
            }

            //METHOD 3: Raycast using colliders with layermask
            // Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            // RaycastHit hit;

            // if (Physics.Raycast(ray, out hit, 100f, clickMask)) {
            //     clickPos = hit.point;
            // }

            //METHOD 3: Raycast using colliders with layermask
            // Plane plane = new Plane(Vector3.up, 0f);
            // Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            // float distanceToPlane;
            // if (plane.Raycast(ray, out distanceToPlane)) {
            //     clickPos = ray.GetPoint(distanceToPlane);
            // }

            // GOAL
            // LOG CLICK POSITION IN WORLD SPACE TO THE CONSOLE
            Debug.Log(clickPos);
        }

    }
}